# Javascript Card Deck Sorter

Given a standard deck of cards (52 cards, 4 suits) the Javascript does the following:
- Shuffles the deck;
- Filters by suit, retaining order;
- Restores initial state (original card order).
